package com.team.enablement.plateform.util;

import com.team.enablement.plateform.ScannerUtil;

import java.util.Locale;

public class Utility
{
	final static String YES = "Y";
	final static String NO = "N";

	public static boolean addMore()
	{
		String addMore;
		do
		{
			System.out.println("\nDo you want to add more Y/N");
			addMore = ScannerUtil.getScanner().nextLine().toUpperCase(Locale.ROOT);
			if (addMore.equals(YES))
			{
				return true;
			} else if (addMore.equals(NO))
			{
				break;
			} else
			{
				System.out.println("\nPlease enter Y/N\n");
			}
		} while (true);
		return false;
	}
}
