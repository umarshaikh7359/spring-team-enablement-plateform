package com.team.enablement.plateform.util;

import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.TimeZone;

import static java.util.TimeZone.getTimeZone;

public class GreetUser
{
	final static String MORNING = "Morning";
	final static String AFTERNOON = "Afternoon";
	final static String EVENING = "Evening";
	final static String FORMAT = "HH:mm:ss   dd/MM/yyyy ";
	final static String[] greetType = {MORNING, AFTERNOON, EVENING};


	public static String greetUser(String timeZone)
	{
		Date Today = new Date();
		Calendar calendar = Calendar.getInstance(getTimeZone(timeZone));
		int hour = calendar.get(Calendar.HOUR_OF_DAY);
		SimpleDateFormat timeDateFormat = new SimpleDateFormat(FORMAT);
		timeDateFormat.setTimeZone(TimeZone.getTimeZone(timeZone));

		System.out.println("\n" + timeDateFormat.format(Today));

		if (hour < 12)
		{
			return greetType[0];
		} else if (hour < 17)
		{
			return greetType[1];
		} else
		{
			return greetType[2];
		}
	}
}


