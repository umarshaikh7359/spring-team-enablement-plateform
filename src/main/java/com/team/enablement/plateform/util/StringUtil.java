package com.team.enablement.plateform.util;

import com.team.enablement.plateform.model.Hashtag;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.Set;
import java.util.stream.Collectors;

public class StringUtil
{
	final static String HASH = "#";
	final static String EMPTY = "";
	final static String SPLIT = "\\s";

	public static List<Long> transformStringToListOfInteger(String s)
	{
		List<String> subs = new ArrayList<>(List.of(s.split(SPLIT)));
		List<Long> ids = new ArrayList<>();
		for (String string : subs)
		{
			if (string.isEmpty())
			{
				continue;
			}
			try
			{
				long id = Long.parseLong(string);
				if (id > 0)
				{
					ids.add(id);
				}
			} catch (NumberFormatException e)
			{
				System.out.println(string + " can't be an id");
			}
		}
		return ids;
	}

	public static boolean isEmptyOrNull(String s)
	{
		return s == null || s.isEmpty();
	}
	public static Set<Hashtag> findHashtagsFromString(String description)
	{
		List<String> descriptionList = new ArrayList<>(List.of(description.split(SPLIT)));

		return descriptionList.stream()
				.filter(s -> !s.isEmpty())
				.filter(s -> s.charAt(0) == HASH.charAt(0))
				.filter(s -> s.length() >= 2)
				.map(hashtagName -> new Hashtag(hashtagName.replace(HASH, EMPTY)))
				.collect(Collectors.toSet());
	}
	public static String removeHashesFromString(String description)
	{
		return description.replace(HASH, EMPTY);
	}

	public static String preparePlaceHolders(int length) {
		return String.join(",", Collections.nCopies(length, "?"));
	}
}
