package com.team.enablement.plateform.model;


import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.util.Objects;
import java.util.Set;

@Data
@NoArgsConstructor
@AllArgsConstructor
public class Topic extends BaseData
{
    long id;

    private String topicTitle;
    private String topicDescription;

    private Set<Hashtag> hashtag;

    public Topic(final Long id, final String topicTitle, final String topicDescription)
    {
        this.id = id;
        this.topicTitle = topicTitle;
        this.topicDescription = topicDescription;
    }

    public Topic(final String topicTitle, final String topicDescription)
    {
        this.topicTitle = topicTitle;
        this.topicDescription = topicDescription;
    }


    @Override
    public boolean equals(Object o)
    {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Topic topic = (Topic) o;
        return topicTitle.equals(topic.topicTitle);
    }

    @Override
    public int hashCode()
    {
        return Objects.hash(topicTitle);
    }

    @Override
    public String toString()
    {
        return  "\n\ntopicId=" + id +
                ",\ntopicTitle='" + topicTitle + '\'' +
                ",\ntopicDescription='" + topicDescription + '\'' +
                ",\nhashtag=" + hashtag;
    }

}


