package com.team.enablement.plateform.model;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.util.List;
import java.util.Objects;


@Data
@NoArgsConstructor
@AllArgsConstructor
public class Deck extends BaseData {
    private int id;
    private String title;
    private String description;
    private List<Slice> slices;
    private List<Topic> topics;

    public Deck(final String title, final String description)
    {
        this.title = title;
        this.description = description;
    }

    @Override
    public boolean equals(final Object o)
    {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Deck deck = (Deck) o;
        return id == deck.id;
    }

    @Override
    public int hashCode()
    {
        return Objects.hash(id);
    }

    @Override
    public String toString()
    {
        return  "\ndeckId = " + id +
                "\ntitle = " + title +
                "\ndescription = " + description +
                "\n slices=" + slices +
                "\n topics=" + topics;
    }
}
