package com.team.enablement.plateform.model;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.util.Set;

@Data
@NoArgsConstructor
@AllArgsConstructor
public class Slice extends BaseData
{
    long id;
    private String sliceDescription;
    public Set<Hashtag> sliceHashtagList;

    public Slice(final String description)
    {
        this.sliceDescription = description;
    }

    public Slice(final Long id, final String sliceDescription)
    {
        this.id = id;
        this.sliceDescription = sliceDescription;
    }


    @Override
    public String toString()
    {
        return  "\n\ntopicId=" + id +
                ",\ntopicDescription='" + sliceDescription + '\'' +
                ",\nhashtag=" + sliceHashtagList;
    }
}
