package com.team.enablement.plateform.model;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.util.Locale;
import java.util.Objects;

@Data
@NoArgsConstructor
@AllArgsConstructor
public class Hashtag extends BaseData
{
    long id;
    private String hashtagName;

    public Hashtag(final String hashtagName)
    {
        this.hashtagName = hashtagName;
    }

    @Override
    public boolean equals(Object o)
    {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Hashtag hashtag1 = (Hashtag) o;
        return hashtagName.equals(hashtag1.hashtagName);
    }

    @Override
    public int hashCode()
    {
        return Objects.hash(hashtagName);
    }

    @Override
    public String toString()
    {
        return  getId() +
                " - \"" + hashtagName +"\" ";
    }

}
