package com.team.enablement.plateform;

import org.springframework.context.ApplicationContext;
import org.springframework.context.annotation.AnnotationConfigApplicationContext;

public class Main
{
	public static void main(String[] args)
	{
		ApplicationContext a = new AnnotationConfigApplicationContext(Config.class);

		Menu menu = a.getBean(Menu.class);
		menu.menu();
	}

}
