package com.team.enablement.plateform;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.team.enablement.plateform.exception.InvalidDeckException;
import com.team.enablement.plateform.exception.InvalidSliceException;
import com.team.enablement.plateform.exception.InvalidTopicException;
import com.team.enablement.plateform.exception.ObjectNotFound;
import com.team.enablement.plateform.exception.UniqueConstraintViolationException;
import com.team.enablement.plateform.model.Topic;
import com.team.enablement.plateform.service.TopicService;
import com.team.enablement.plateform.util.Utility;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import java.io.IOException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Comparator;
import java.util.List;
import java.util.Scanner;

@Component
public class TopicOperations
{
	private static final Logger logger = LogManager.getLogger(TopicOperations.class);
	static Scanner sc = ScannerUtil.getScanner();
	@Autowired
	TopicService topicService;

	public TopicOperations(final TopicService topicService)
	{
		this.topicService = topicService;
	}

	public void doOperations()
	{
		long selectTopicOperation;
		do
		{
			System.out.println("\nPlease choose one operation");
			System.out.println("1. Create Topic \n2. View all topic \n3. View topic by topic Id\n4. Go Back");

			selectTopicOperation = ScannerUtil.UserInput();

			if (selectTopicOperation == 1)
			{
				do
				{
					do
					{
						System.out.println("Enter the title of the topic: ");
						String topicTitle = sc.nextLine();
						System.out.println("Enter the description of the topic: ");
						String topicDescription = sc.nextLine();
						Topic topic = new Topic(topicTitle, topicDescription);

						try
						{
							if (topicService.save(topic) != null)
							{
								System.out.println("\n Topic successfully created!");
								logger.info("Topic successfully created for Id : {} & title : {} & description : {}", topic.getId(), topic.getTopicTitle(), topic.getTopicDescription());
								break;
							}
						} catch (InvalidTopicException e)
						{
							logger.error("Invalid topic title : {} or description : {}", topic.getTopicTitle(), topic.getTopicDescription());
							System.out.println("\nValidation fails " + e.getMessage());
						} catch (UniqueConstraintViolationException e)
						{
							logger.error("Topic title : {} already exist", topic.getTopicTitle());
							System.out.println("\nTopic title : " + topic.getTopicTitle() + " already exist\n");
						} catch (InvalidDeckException | ObjectNotFound | InvalidSliceException e)
						{
							throw new RuntimeException(e);
						}
					} while (true);

					displayAllTopics();

				} while (Utility.addMore());

			} else if (selectTopicOperation == 2)
			{
				displayAllTopics();
			} else if (selectTopicOperation == 3)
			{
				System.out.println("Enter Id of the topic");
				Long id = ScannerUtil.UserInput();
//				try
//				{
					Topic topic = topicService.findById(id);
					System.out.println(topic);
					logger.info("Topic successfully displayed for Id : {}", id);
				//}
//				catch (ObjectNotFound e)
//				{
//					System.out.println("Topic Not found for Id :" + id);
//					logger.error("\n No topic was found for Id : {}", id);
//				} catch (ConnectionFails e)
//				{
//					logger.error("Connection failed", e);
//					System.out.println("\nConnection failed ");
//				}
			} else if (selectTopicOperation == 4)
			{
				break;
			} else
			{
				System.out.println("Please enter from above options");
			}
		} while (true);
	}

	public void displayAllTopics()
	{
		Comparator<Topic> idSorter = Comparator.comparingLong(Topic::getId);
		List<Topic> topics = new ArrayList<>(topicService.findAll());
		if (topics.isEmpty())
		{
			System.out.println("\nNo topics available");
			return;
		}

		topics.sort(idSorter);
		System.out.println("\nAll the topics: ");
		topics.forEach(System.out::println);
	}
}
