package com.team.enablement.plateform.dao;

import com.team.enablement.plateform.model.Hashtag;

import java.util.Set;

public interface HashtagDao
{
	// save hashtags
	Set<Hashtag> saveAll(Set<Hashtag> hashtags);

	Hashtag getHashByName(String hashtagName);
}
