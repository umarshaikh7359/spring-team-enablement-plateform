package com.team.enablement.plateform.dao;

import com.team.enablement.plateform.exception.InvalidSliceException;
import com.team.enablement.plateform.exception.ObjectNotFound;
import com.team.enablement.plateform.model.Slice;

public interface SliceDao extends CurdDao<Slice, Long>
{
	// save slice
	boolean update(Slice slice) throws InvalidSliceException, ObjectNotFound;
}
