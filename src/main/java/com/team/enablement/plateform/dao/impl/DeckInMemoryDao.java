package com.team.enablement.plateform.dao.impl;

import com.team.enablement.plateform.dao.DeckDao;
import com.team.enablement.plateform.exception.ObjectNotFound;
import com.team.enablement.plateform.model.Slice;
import com.team.enablement.plateform.model.Topic;
import com.team.enablement.plateform.service.SliceService;
import com.team.enablement.plateform.service.TopicService;
import com.team.enablement.plateform.model.Deck;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.stereotype.Repository;

import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.List;
import java.util.Objects;
import java.util.Set;

@Repository
public class DeckInMemoryDao implements DeckDao
{
	private static final Logger logger = LogManager.getLogger(DeckInMemoryDao.class);
	static List<Deck> deckList = new ArrayList<>();
	SliceService sliceService;

	TopicService topicService;

	//Saving deck in storage
	public Deck save(Deck deck) throws ObjectNotFound
	{
		deck.setId(deckList.size() + 1);

		ifExistsTopic(deck);
		ifExistsSlice(deck);

		deck.setCreateDate(LocalDateTime.now());


		logger.info("Deck saved successfully in storage with id : {} & title : {}", deck.getId(), deck.getTitle());
		deckList.add(deck);
		return deck;
	}

	@Override
	public Deck findById(final Long id)
	{
		return deckList.stream()
				.filter(deck -> deck.getId() == id).findFirst().orElse(null);
	}

	public Set<Deck> findAll()
	{
		return (Set<Deck>) deckList;
	}

	public void ifExistsTopic(Deck deck) throws ObjectNotFound
	{

		for (Topic topic : deck.getTopics())
		{
			Topic topicFromDb = topicService.findById(topic.getId());
			if (topicFromDb == null)
			{
				logger.info("Topic doesn't exist with id : {}", topic.getId());
				throw new ObjectNotFound("Topic doesn't exist with id :" + topic.getId());
			} else
			{
				topic.setTopicTitle(topicFromDb.getTopicTitle());
				topic.setTopicDescription(topicFromDb.getTopicDescription());
			}
		}
	}

	public void ifExistsSlice(Deck deck) throws ObjectNotFound
	{
		for (Slice slice : deck.getSlices())
		{
			Slice sliceFromDb = sliceService.findById(slice.getId());
			if (sliceFromDb == null)
			{
				logger.info("Slice doesn't exist with id : {}", slice.getId());
				throw new ObjectNotFound("Slice doesn't exist with id :" + slice.getId());
			} else
			{
				slice.setSliceDescription(sliceFromDb.getSliceDescription());
			}
		}
	}


}
