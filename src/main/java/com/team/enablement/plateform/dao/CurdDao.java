package com.team.enablement.plateform.dao;

import com.team.enablement.plateform.exception.ObjectNotFound;
import com.team.enablement.plateform.exception.UniqueConstraintViolationException;

import java.util.Set;

public interface CurdDao<T, ID>
{
	T save(T object) throws UniqueConstraintViolationException, ObjectNotFound;// InvalidDeckException, ObjectNotFound, InvalidSliceException, InvalidTopicException,

	T findById(ID id);

	Set<T> findAll();
}
