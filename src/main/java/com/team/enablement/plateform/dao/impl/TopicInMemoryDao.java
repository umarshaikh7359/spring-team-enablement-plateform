package com.team.enablement.plateform.dao.impl;

import com.team.enablement.plateform.dao.TopicDao;
import com.team.enablement.plateform.exception.UniqueConstraintViolationException;
import com.team.enablement.plateform.model.Topic;
import org.springframework.stereotype.Repository;

import java.util.HashSet;
import java.util.Set;

@Repository
public class TopicInMemoryDao implements TopicDao
{
	private static final Set<Topic> topicSet = new HashSet<>();

	public Topic save(Topic topic) throws UniqueConstraintViolationException
	{
		topic.setId((topicSet.size() + 1));
		validate(topic);
		topicSet.add(topic);
		return topic;
	}

	@Override
	public Topic findById(final Long topicId)
	{
		return topicSet.stream().filter(topic -> topic.getId() == topicId).findFirst().orElse(null);
	}

	public void validate(Topic topic) throws UniqueConstraintViolationException
	{
		if (topicSet.contains(topic))
		{
			throw new UniqueConstraintViolationException("\nTopic title already exists \n");
		}
	}

	public Set<Topic> findAll()
	{
		if (topicSet.isEmpty())
		{
			return null;
		}
		return topicSet;
	}

}