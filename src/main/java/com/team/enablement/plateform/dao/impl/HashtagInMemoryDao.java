package com.team.enablement.plateform.dao.impl;

import com.team.enablement.plateform.dao.HashtagDao;

import com.team.enablement.plateform.model.Hashtag;
import org.springframework.stereotype.Repository;

import java.util.HashSet;

import java.util.Set;
import java.util.stream.Collectors;

@Repository
public class HashtagInMemoryDao implements HashtagDao
{

	public static final Set<Hashtag> hashtagSet = new HashSet<>();


	public Set<Hashtag> saveAll(Set<Hashtag> hashtags)
	{
		hashtags.forEach(topicHashtag -> {
			if (hashtagSet.contains(topicHashtag))
			{
				topicHashtag.setId(hashtagSet.stream().filter(hashtag -> hashtag.equals(topicHashtag)).findFirst().orElseThrow().getId());
			} else
			{
				topicHashtag.setId((long) (hashtagSet.size() + 1));
				hashtagSet.add(topicHashtag);
			}
		});
		return hashtags;
	}

	public Hashtag getHashByName(String hashtagName)
	{
		return hashtagSet.stream()
				.filter(hashtag1 -> hashtagName.equals(hashtag1.getHashtagName()))
				.findFirst().orElse(null);
//		if (hashtag == null)
//		{
//			Hashtag newHashtag = new Hashtag(hashtags.size() + 1, hashtagName); // new hashtag object with id and name
//			newHashtag.setCreateDate(LocalDateTime.now());// assignment of create date
//			HashtagStorage.hashtags.add(newHashtag); // add hashtag
//			return newHashtag;
//		} else
//		{
//			return hashtag;
//		}
	}

	public Set<Hashtag> assignIdToHashtags(Set<Hashtag> hashtags)
	{
		return hashtagSet
				.stream()
				.filter(hashtag -> hashtags.stream().anyMatch((hashtag::equals)))
				.collect(Collectors.toSet());
	}
}
