package com.team.enablement.plateform.dao;

import com.team.enablement.plateform.exception.InvalidTopicException;
import com.team.enablement.plateform.exception.UniqueConstraintViolationException;
import com.team.enablement.plateform.model.Topic;

public interface TopicDao extends CurdDao<Topic, Long>
{
	Topic save(Topic topic) throws UniqueConstraintViolationException;

	void validate(Topic topic) throws InvalidTopicException, UniqueConstraintViolationException;
}

