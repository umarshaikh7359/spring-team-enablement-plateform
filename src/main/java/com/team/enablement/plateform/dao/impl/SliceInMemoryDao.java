package com.team.enablement.plateform.dao.impl;

import com.team.enablement.plateform.dao.SliceDao;
import com.team.enablement.plateform.exception.InvalidSliceException;
import com.team.enablement.plateform.model.Slice;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.stereotype.Repository;

import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

@Repository
public class SliceInMemoryDao implements SliceDao
{
	private static final Logger logger = LogManager.getLogger();

	public static List<Slice> slices = new ArrayList<>(); // SLICES

	public Slice save(Slice slice)
	{
		slice.setId(slices.size() + 1);// Give id to Slice
		slice.setCreateDate(LocalDateTime.now());
		// Adding Slice to memory
		slices.add(slice);
		logger.info("Exiting addSliceToList method");
		return slice;
	}

	@Override
	public Slice findById(final Long id)
	{
		logger.info("Entering searchSlice method");
		// Search in InMemory
		return slices.stream().filter(slice -> slice.getId() == id)
				.findFirst().orElse(null);
	}

	// #awe #asd #asdr #hetrhrt #arhethw #sweg
	public Set<Slice> findAll()
	{
		return new HashSet<>(slices);// return static Slice List
	}

	public boolean update(Slice slice)
	{
		logger.info("Entering updateSliceInMemory method");
		slice.setUpdateDate(LocalDateTime.now());
		slices.set((int) (slice.getId() - 1), slice);
		logger.info("Exiting updateSliceInMemory method");
		return true;
	}

	public boolean checkIfSliceExist(int id)
	{
		return slices.stream().noneMatch(slice -> slice.getId() == id);
	}
}