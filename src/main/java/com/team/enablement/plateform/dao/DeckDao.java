package com.team.enablement.plateform.dao;

import com.team.enablement.plateform.exception.InvalidDeckException;
import com.team.enablement.plateform.exception.ObjectNotFound;
import com.team.enablement.plateform.model.Deck;


public interface DeckDao extends CurdDao<Deck, Long>
{
	void ifExistsTopic(Deck deck) throws ObjectNotFound;

	void ifExistsSlice(Deck deck) throws ObjectNotFound;

}
