package com.team.enablement.plateform;

import com.team.enablement.plateform.dao.DeckDao;
import com.team.enablement.plateform.dao.HashtagDao;
import com.team.enablement.plateform.dao.SliceDao;
import com.team.enablement.plateform.dao.TopicDao;
import com.team.enablement.plateform.dao.impl.DeckInMemoryDao;
import com.team.enablement.plateform.dao.impl.HashtagInMemoryDao;
import com.team.enablement.plateform.dao.impl.SliceInMemoryDao;
import com.team.enablement.plateform.dao.impl.TopicInMemoryDao;
import com.team.enablement.plateform.model.Hashtag;
import com.team.enablement.plateform.service.DeckService;
import com.team.enablement.plateform.service.HashtagService;
import com.team.enablement.plateform.service.SliceService;
import com.team.enablement.plateform.service.TopicService;
import com.team.enablement.plateform.service.impl.DeckServiceImpl;
import com.team.enablement.plateform.service.impl.HashtagServiceImpl;
import com.team.enablement.plateform.service.impl.SliceServiceImpl;
import com.team.enablement.plateform.service.impl.TopicServiceImpl;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

@Configuration
class Config
{
	@Bean
	Menu menuBean(TopicOperations topicOperations, SliceOperations sliceOperations, DeckOperations deckOperations) {
		return new Menu(topicOperations, sliceOperations, deckOperations);
	}

	// Operation--------------------------------------------------------------------------
	@Bean
	TopicOperations topicOperationsBean(TopicService topicService) {
		return new TopicOperations(topicService);
	}
	@Bean
	SliceOperations sliceOperationsBean(SliceService sliceService) {
		return new SliceOperations(sliceService);
	}
	@Bean
	DeckOperations deckOperationsBean(DeckService deckService) {
		return new DeckOperations(deckService);
	}
	//-----------------------------------------------------------------------------


	// Service---------------------------------------------------------------------
	@Bean
	TopicService topicServiceBean(TopicDao topicDao, HashtagService hashtagService) {
		return new TopicServiceImpl(topicDao, hashtagService);
	}
	@Bean
	HashtagService hashtagServiceBean(HashtagDao hashtagDao) {
		return new HashtagServiceImpl(hashtagDao);
	}
	@Bean
	SliceService sliceServiceBean(SliceDao sliceDao, HashtagService hashtagService) {
		return new SliceServiceImpl(hashtagService, sliceDao);
	}
	@Bean
	DeckService deckServiceBean(SliceService sliceService, final TopicService topicService, final DeckDao deckDao) {
		return new DeckServiceImpl(sliceService, topicService, deckDao);
	}
	//----------------------------------------------------------------------------------------

	// Dao -------------------------------------------------------------------------------------
	@Bean
	TopicDao topicDaoBean() {
		return new TopicInMemoryDao();
	}
	@Bean
	HashtagDao hashtagDaoBean() {
		return new HashtagInMemoryDao();
	}
	@Bean
	SliceDao sliceDaoBean() {
		return new SliceInMemoryDao();
	}
	@Bean
	DeckDao deckDaoBean() {
		return new DeckInMemoryDao();
	}
	//------------------------------------------------------------------------------------------------------
}