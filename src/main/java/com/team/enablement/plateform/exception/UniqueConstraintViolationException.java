package com.team.enablement.plateform.exception;

public class UniqueConstraintViolationException extends Exception
{
	public UniqueConstraintViolationException(String string)
	{
		super(string);
	}
}
