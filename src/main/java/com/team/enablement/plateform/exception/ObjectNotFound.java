package com.team.enablement.plateform.exception;

public class ObjectNotFound extends Exception
{

	public ObjectNotFound(String string)
	{

		super(string);
	}
}
