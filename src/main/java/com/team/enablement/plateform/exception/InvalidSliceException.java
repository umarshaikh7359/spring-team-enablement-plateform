package com.team.enablement.plateform.exception;

public class InvalidSliceException extends Exception
{
	public InvalidSliceException(String message)
	{
		super(message);
	}
}
