package com.team.enablement.plateform.exception;

public class InvalidTopicException extends Exception {

        public InvalidTopicException (String str)
        {
            super(str);
        }
}
