package com.team.enablement.plateform.exception;

public class InvalidDeckException extends Exception
{
	public InvalidDeckException(String string)
	{
		super(string);
	}
}
