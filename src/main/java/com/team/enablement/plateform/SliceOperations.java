package com.team.enablement.plateform;

import com.team.enablement.plateform.exception.InvalidDeckException;
import com.team.enablement.plateform.exception.InvalidSliceException;
import com.team.enablement.plateform.exception.InvalidTopicException;
import com.team.enablement.plateform.exception.ObjectNotFound;
import com.team.enablement.plateform.exception.UniqueConstraintViolationException;
import com.team.enablement.plateform.model.Slice;
import com.team.enablement.plateform.service.SliceService;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import java.time.LocalDateTime;
import java.util.Scanner;
import java.util.Set;

@Component
public class SliceOperations
{
	private static final Logger logger = LogManager.getLogger();
	@Autowired
	SliceService sliceService;
	static Scanner sc = ScannerUtil.getScanner();

	public SliceOperations(final SliceService sliceService)
	{
		this.sliceService = sliceService;
	}

	// Slice Menu...
	public void doOperations()
	{
		long userChoice = 0;
		System.out.println("\nPlease choose one operation");
		while (userChoice != 4)
		{
			System.out.println("1. Create Slice \n2. Display All Slices \n3. Update Slice \n4. Go Back\n");
			System.out.println("Enter your Choice");

			logger.info("Entering in the Slice Switch");
			switch ((int) (userChoice = ScannerUtil.UserInput()))
			{
				case 1:
					inputForCreateSlice();
					break;
				case 2:
					displayAllSlices();
					break;
				case 3:
					inputForUpdate();
					break;
				case 4:
					break;
				default:
					System.out.println("Please Enter valid Value");
					break;
			}
			logger.info("Exiting the Slice Switch");
		}
	}

	private void inputForCreateSlice()
	{
		logger.info("Request received for create Slice at : {}", LocalDateTime.now());
		while (true)
		{
			System.out.println("Enter Slice Description");
			String description = sc.nextLine();
			logger.info("Received request for create Slice for description : {}", description);
			try
			{
				if (sliceService.save(new Slice(description)) != null)
				{
					System.out.println("Slice is Successfully created\n");
					break;
				}
			} catch (InvalidSliceException ex)
			{
				System.out.println(ex.getMessage());
			} catch (InvalidDeckException | ObjectNotFound | InvalidTopicException |
			         UniqueConstraintViolationException e)
			{
				throw new RuntimeException(e);
			}
		}
		logger.info("Request responded for create Slice at : {}", LocalDateTime.now());
	}

	public void displayAllSlices()
	{
		// Get Slices From DB..
		Set<Slice> slices = sliceService.findAll();

		logger.info("Printing List of Slices");

		if (slices.isEmpty())
		{
			System.out.println("Sorry no Slices are Created yet...\n");
		} else
		{
			System.out.println("List of All Slices...");
			// Display for Slices
			slices.forEach(System.out::println);
		}
	}

	public void inputForUpdate()
	{
		logger.info("Request received for update Slice at : {}", LocalDateTime.now());
		if (sliceService.findAll().isEmpty())
		{
			System.out.println("Sorry List is empty you can't update any Slices..\n");
			return;
		}
		while (true)
		{
			System.out.println("Enter Id for Slice you want to update OR '0' -> back to Menu");
			long sliceId = ScannerUtil.UserInput();
			if (sliceId == 0)
			{
				break;
			}
			System.out.println("Enter New Slice Description:");
			String newDescription = sc.nextLine();
			logger.info("Received request for update slice for id : {} & description : {}", sliceId, newDescription);
			try
			{
				if (sliceService.update(new Slice(sliceId, newDescription)))
				{
					System.out.println("Slice is Successfully updated\n");
					break;
				}
			} catch (InvalidSliceException | ObjectNotFound ex)
			{
				logger.error(ex.getMessage());
				System.out.println(ex.getMessage());
			}
		}
		logger.info("Request responded for update Slice at : {}", LocalDateTime.now());
	}

}
