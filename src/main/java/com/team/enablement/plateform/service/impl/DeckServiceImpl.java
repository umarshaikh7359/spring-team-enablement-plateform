package com.team.enablement.plateform.service.impl;

import com.team.enablement.plateform.dao.DeckDao;
import com.team.enablement.plateform.exception.InvalidDeckException;
import com.team.enablement.plateform.exception.InvalidSliceException;
import com.team.enablement.plateform.exception.InvalidTopicException;
import com.team.enablement.plateform.exception.UniqueConstraintViolationException;
import com.team.enablement.plateform.model.Slice;
import com.team.enablement.plateform.model.Topic;
import com.team.enablement.plateform.service.DeckService;
import com.team.enablement.plateform.exception.ObjectNotFound;
import com.team.enablement.plateform.model.Deck;
import com.team.enablement.plateform.service.SliceService;
import com.team.enablement.plateform.service.TopicService;
import com.team.enablement.plateform.util.StringUtil;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.Set;

@Service
public class DeckServiceImpl implements DeckService
{
	private static final Logger logger = LogManager.getLogger(DeckService.class);
	@Autowired
	SliceService sliceService;
	@Autowired
	TopicService topicService;
	@Autowired
	DeckDao deckDao;

	public DeckServiceImpl(final SliceService sliceService, final TopicService topicService, final DeckDao deckDao)
	{
		this.sliceService = sliceService;
		this.topicService = topicService;
		this.deckDao = deckDao;
	}

	@Override
	public Deck save(final Deck deck) throws InvalidDeckException, ObjectNotFound, InvalidSliceException, InvalidTopicException, UniqueConstraintViolationException
	{
		//Validate title and description
		validate(deck);

		//Save deck in storage class
		return deckDao.save(deck);
	}

	@Override
	public Deck findById(final Long id)
	{
		return deckDao.findById(id);
	}

	@Override
	public Set<Deck> findAll()
	{
		return null;
	}

	@Override
	public boolean validate(final Deck deck) throws InvalidDeckException, ObjectNotFound
	{
		//Check if title or description is null
		if (StringUtil.isEmptyOrNull(deck.getTitle()) || StringUtil.isEmptyOrNull(deck.getDescription()))
		{
			logger.error("Invalidated deck title : {} or deck description : {}", deck.getTitle(), deck.getDescription());
			throw new InvalidDeckException("Title or description should not be null or empty");
		}

		//Check valid length of title and description
		else if (deck.getTitle().length() > 100 || deck.getDescription().length() > 1000)
		{
			logger.error("Invalidated length of deck title : {} or deck description : {}", deck.getTitle(), deck.getDescription());
			throw new InvalidDeckException("Please enter valid length of title and description");
		}
		ifExistsTopic(deck);
		ifExistsSlice(deck);
		logger.info("Deck validated successfully for title : {} & description : {}", deck.getTitle(), deck.getDescription());
		return true;
	}

	@Override
	public void ifExistsTopic(final Deck deck) throws ObjectNotFound
	{
		for (Topic topic : deck.getTopics())
		{
			Topic topicFromDb = topicService.findById(topic.getId());
			if (topicFromDb == null)
			{
				logger.info("Topic doesn't exist with id : {}", topic.getId());
				throw new ObjectNotFound("Topic doesn't exist with id :" + topic.getId());
			} else
			{
				topic.setTopicTitle(topicFromDb.getTopicTitle());
				topic.setTopicDescription(topicFromDb.getTopicDescription());
			}
		}
	}

	@Override
	public void ifExistsSlice(final Deck deck) throws ObjectNotFound
	{
		for (Slice slice : deck.getSlices())
		{
			Slice sliceFromDb = sliceService.findById(slice.getId());
			if (sliceFromDb == null)
			{
				logger.info("Slice doesn't exist with id : {}", slice.getId());
				throw new ObjectNotFound("Slice doesn't exist with id :" + slice.getId());
			} else
			{
				slice.setSliceDescription(sliceFromDb.getSliceDescription());
			}
		}
	}
}
