package com.team.enablement.plateform.service;

import com.team.enablement.plateform.exception.InvalidDeckException;
import com.team.enablement.plateform.exception.InvalidSliceException;
import com.team.enablement.plateform.exception.InvalidTopicException;
import com.team.enablement.plateform.exception.ObjectNotFound;
import com.team.enablement.plateform.exception.UniqueConstraintViolationException;

import java.util.Set;

public interface CrudService<T, ID>
{
	T save(T object) throws InvalidDeckException, ObjectNotFound, InvalidSliceException, InvalidTopicException, UniqueConstraintViolationException;

	T findById(ID id);

	Set<T> findAll();
}
