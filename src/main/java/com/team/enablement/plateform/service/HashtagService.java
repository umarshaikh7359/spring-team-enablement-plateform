package com.team.enablement.plateform.service;

import com.team.enablement.plateform.model.Hashtag;

import java.util.Set;

public interface HashtagService
{
	boolean saveAll(Set<Hashtag> hashtags);

	Hashtag getHashByName(String hashtagName);
}
