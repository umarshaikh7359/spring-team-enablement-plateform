package com.team.enablement.plateform.service.impl;

import com.team.enablement.plateform.dao.SliceDao;
import com.team.enablement.plateform.exception.InvalidDeckException;
import com.team.enablement.plateform.exception.InvalidSliceException;
import com.team.enablement.plateform.exception.InvalidTopicException;
import com.team.enablement.plateform.exception.ObjectNotFound;
import com.team.enablement.plateform.exception.UniqueConstraintViolationException;
import com.team.enablement.plateform.model.Slice;
import com.team.enablement.plateform.service.HashtagService;
import com.team.enablement.plateform.service.SliceService;
import com.team.enablement.plateform.util.StringUtil;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.Set;

@Service
public class SliceServiceImpl implements SliceService
{
	private static final Logger logger = LogManager.getLogger();
	@Autowired
	HashtagService hashtagService;
	@Autowired
	SliceDao sliceDao;

	public SliceServiceImpl(final HashtagService hashtagService, final SliceDao sliceDao)
	{
		this.hashtagService = hashtagService;
		this.sliceDao = sliceDao;
	}

	// Create Slice in DB/In_Memory
	public Slice save(Slice slice) throws InvalidSliceException, InvalidDeckException, ObjectNotFound, InvalidTopicException, UniqueConstraintViolationException
	{
		//  get List of Hashtag of Slice
		slice.setSliceHashtagList(StringUtil.findHashtagsFromString(slice.getSliceDescription()));

		// remove # from description...
		slice.setSliceDescription(StringUtil.removeHashesFromString(slice.getSliceDescription()));

		validateSlice(slice);
		// If validation true then save it
		hashtagService.saveAll(slice.getSliceHashtagList());
//
		return sliceDao.save(slice);
	}

	public void validateSlice(Slice slice) throws InvalidSliceException
	{
		if (StringUtil.isEmptyOrNull(slice.getSliceDescription()))
		{
			logger.error("Invalidated Slice description : {}", slice.getSliceDescription());
			throw new InvalidSliceException("Description is Empty or Null");
		}

		// Slice description Should be less than 200 char...
		else if (slice.getSliceDescription().length() > 200)
		{
			logger.error("Invalidated Slice description length : {}", slice.getSliceDescription().length());
			throw new InvalidSliceException("Length of Description is:" + slice.getSliceDescription().length() + "\nMore than 200 is not valid length");
		}
	}

	public boolean update(Slice slice) throws InvalidSliceException, ObjectNotFound
	{
		// Find input Id in DB, return Slice obj
		if (sliceDao.findById(slice.getId()) == null)
		{
			logger.error("Slice id : {}", slice.getSliceDescription() + " Not Found");
			throw new ObjectNotFound("Slice doesn't exist with id: " + slice.getId());
		}

		// re-Process on NEW Description
		slice.setSliceHashtagList(StringUtil.findHashtagsFromString(slice.getSliceDescription()));// Hashtags List
		slice.setSliceDescription(StringUtil.removeHashesFromString(slice.getSliceDescription()));// remove #

		validateSlice(slice);
		// update Slice
		hashtagService.saveAll(slice.getSliceHashtagList());
		sliceDao.update(slice);
		return true;
	}

	public Slice findById(Long id) //throws ObjectNotFound, ConnectionFails
	{
		logger.info("Received request to find slice by Id : {} ", id);
		return sliceDao.findById(id);
	}

	public Set<Slice> findAll() //throws ConnectionFails
	{
		return sliceDao.findAll();
	}
}
