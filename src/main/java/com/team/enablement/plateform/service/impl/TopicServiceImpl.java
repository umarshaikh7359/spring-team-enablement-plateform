package com.team.enablement.plateform.service.impl;


import com.team.enablement.plateform.dao.TopicDao;
import com.team.enablement.plateform.exception.InvalidDeckException;
import com.team.enablement.plateform.exception.InvalidTopicException;
import com.team.enablement.plateform.exception.ObjectNotFound;
import com.team.enablement.plateform.exception.UniqueConstraintViolationException;
import com.team.enablement.plateform.model.Topic;
import com.team.enablement.plateform.service.HashtagService;
import com.team.enablement.plateform.service.TopicService;
import com.team.enablement.plateform.util.StringUtil;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.Set;

@Service
public class TopicServiceImpl implements TopicService
{

	private static final Logger logger = LogManager.getLogger(TopicService.class);

	@Autowired
	TopicDao topicDao;
	@Autowired
	HashtagService hashtagService;

	public TopicServiceImpl(final TopicDao topicDao, final HashtagService hashtagService)
	{
		this.topicDao = topicDao;
		this.hashtagService = hashtagService;
	}

	@Override
	public Topic save(final Topic topic) throws InvalidTopicException, UniqueConstraintViolationException
	{
		topic.setHashtag(StringUtil.findHashtagsFromString(topic.getTopicDescription()));
		topic.setTopicDescription(StringUtil.removeHashesFromString(topic.getTopicDescription()));
		logger.info("Received request to create topic for title : {} & description : {}", topic.getTopicTitle(), topic.getTopicDescription());

		validate(topic);
		hashtagService.saveAll(topic.getHashtag());

		return topicDao.save(topic);
	}

	@Override
	public Topic findById(final Long id)
	{
		logger.info("Received request to find topic by Id : {} ", id);
		return topicDao.findById(id);
	}

	@Override
	public Set<Topic> findAll()
	{
		logger.info("Received request to get all topics ");
		return topicDao.findAll();
	}

	@Override
	public void validate(final Topic topic) throws InvalidTopicException
	{
		if (StringUtil.isEmptyOrNull(topic.getTopicTitle()) || StringUtil.isEmptyOrNull(topic.getTopicDescription()))
		{
			throw new InvalidTopicException("Topic title or description is either null or empty ");
		} else if (topic.getTopicTitle().length() > 100 || topic.getTopicDescription().length() > 1000)
		{
			throw new InvalidTopicException("Topic title or description is not valid");
		}
	}
}
