package com.team.enablement.plateform.service;


import com.team.enablement.plateform.exception.InvalidSliceException;
import com.team.enablement.plateform.exception.ObjectNotFound;
import com.team.enablement.plateform.model.Slice;

public interface SliceService extends CrudService<Slice, Long>
{
	boolean update(Slice slice) throws InvalidSliceException, ObjectNotFound;

	void validateSlice(Slice slice) throws InvalidSliceException;
}
