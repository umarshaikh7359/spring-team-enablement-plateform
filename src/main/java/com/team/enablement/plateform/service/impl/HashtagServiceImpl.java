package com.team.enablement.plateform.service.impl;

import com.team.enablement.plateform.dao.HashtagDao;
import com.team.enablement.plateform.model.Hashtag;
import com.team.enablement.plateform.service.HashtagService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.Set;

@Service
public class HashtagServiceImpl implements HashtagService
{
	@Autowired
	HashtagDao hashtagDao;

	public HashtagServiceImpl(final HashtagDao hashtagDao)
	{
		this.hashtagDao = hashtagDao;
	}

	@Override
	public boolean saveAll(final Set<Hashtag> hashtags)
	{
		return hashtagDao.saveAll(hashtags) != null;
	}


	@Override
	public Hashtag getHashByName(final String hashtagName)
	{
		return hashtagDao.getHashByName(hashtagName);
	}
}
