package com.team.enablement.plateform.service;


import com.team.enablement.plateform.exception.InvalidTopicException;
import com.team.enablement.plateform.model.Topic;

public interface TopicService extends CrudService<Topic, Long>
{
	void validate(Topic topic) throws InvalidTopicException;
}
