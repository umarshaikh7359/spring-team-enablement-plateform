package com.team.enablement.plateform.service;

import com.team.enablement.plateform.exception.InvalidDeckException;
import com.team.enablement.plateform.exception.ObjectNotFound;
import com.team.enablement.plateform.model.Deck;

public interface DeckService extends CrudService<Deck, Long>
{
	void ifExistsTopic(Deck deck) throws ObjectNotFound;

	void ifExistsSlice(Deck deck) throws ObjectNotFound;

	boolean validate(final Deck deck) throws InvalidDeckException, ObjectNotFound;
}
