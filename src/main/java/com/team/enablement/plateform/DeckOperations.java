package com.team.enablement.plateform;

import com.team.enablement.plateform.exception.InvalidDeckException;
import com.team.enablement.plateform.exception.InvalidSliceException;
import com.team.enablement.plateform.exception.InvalidTopicException;
import com.team.enablement.plateform.exception.ObjectNotFound;
import com.team.enablement.plateform.exception.UniqueConstraintViolationException;
import com.team.enablement.plateform.model.Deck;
import com.team.enablement.plateform.model.Slice;
import com.team.enablement.plateform.model.Topic;
import com.team.enablement.plateform.service.DeckService;
import com.team.enablement.plateform.util.StringUtil;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.stereotype.Component;

import java.util.List;
import java.util.Scanner;
import java.util.Set;
import java.util.stream.Collectors;
@Component
public class DeckOperations
{
	private static final Logger logger = LogManager.getLogger(DeckOperations.class);
	static Scanner sc = ScannerUtil.getScanner();

	DeckService deckService;

	public DeckOperations(final DeckService deckService)
	{
		this.deckService = deckService;
	}

	// Deck Menu...
	public void doOperations()
	{
		long userChoice = 0;
		System.out.println("Welcome to Deck Menu");
		while (userChoice != 3)
		{
			System.out.println("\n1. Create Deck \n2. Display All Decks \n3. Go Back\n");
			System.out.println("Enter your Choice");

			logger.info("Entering in the Deck Switch");
			switch ((int) (userChoice = ScannerUtil.UserInput()))
			{
				case 1:
					inputDeck();
					break;
				case 2:
					displayDeck();
					break;
				case 3:
					break;
				default:
					System.out.println("Please Enter valid Value");
					break;
			}
			logger.info("Exiting the Deck Switch");
		}
	}

	public void inputDeck()
	{
		boolean createDeck = false;
		while (!createDeck)
		{

			//Ask for title, description, topic ids, slice ids
			System.out.print("Enter Deck Title: ");
			String title = sc.nextLine();
			System.out.print("Enter Deck Description: ");
			String description = sc.nextLine();
			System.out.print("Enter topicIds: ");
			String topicIdsString = sc.nextLine();
			System.out.print("Enter sliceIds: ");
			String sliceIdsString = sc.nextLine();

			//Take topic ids and validate it
			List<Long> topicIds = StringUtil.transformStringToListOfInteger(topicIdsString);

			//Take slice ids and validate it
			List<Long> sliceIds = StringUtil.transformStringToListOfInteger(sliceIdsString);

			logger.info("Received request for create deck for title : {} & description : {}", title, description);

			//Create new deck
			Deck deck = new Deck(title, description);

			deck.setTopics(topicIds.stream().
					map(t -> new Topic(t,"","")).
					collect(Collectors.toList()));

			deck.setSlices(sliceIds.stream().
					map(s -> new Slice(s,"")).
					collect(Collectors.toList()));

			//Call createDeck method
			try
			{
				createDeck = deckService.save(deck) != null;
			} catch (InvalidDeckException | ObjectNotFound e)
			{
				logger.error(e.getMessage());
				System.out.println("\n" + e.getMessage() + "\n");
			} catch (InvalidSliceException | InvalidTopicException | UniqueConstraintViolationException e)
			{
				throw new RuntimeException(e);
			}
		}
	}

	public void displayDeck()
	{
		Set<Deck> decks;
		decks = deckService.findAll();
		if (decks.isEmpty())
		{
			System.out.println("There are no deck.");
		} else
		{
			decks.forEach(System.out::println);
			logger.info("Deck successfully displayed");
		}
	}

}


