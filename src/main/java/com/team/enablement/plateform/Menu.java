package com.team.enablement.plateform;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import static java.lang.System.exit;

@Component
public class Menu
{
	@Autowired
	TopicOperations topicOperations;

	@Autowired
	SliceOperations sliceOperations;

	@Autowired
	DeckOperations deckOperations;

	public Menu(final TopicOperations topicOperations, final SliceOperations sliceOperations, final DeckOperations deckOperations)
	{
		this.topicOperations = topicOperations;
		this.sliceOperations = sliceOperations;
		this.deckOperations = deckOperations;
	}

	void menu()
	{
		WelcomePage.welcomePage();
		do
		{
			System.out.println("\nEnter the number for the entity you wish to create: ");
			System.out.println("1. Topic \n2. Slice \n3. Deck \n4. Exit");
			long selectEntity = ScannerUtil.UserInput();
			switch ((int) selectEntity)
			{
				case 1 -> topicOperations.doOperations();
				case 2 -> sliceOperations.doOperations();
				case 3 -> deckOperations.doOperations();
				case 4 -> exit(0);
				default -> System.out.println("Invalid value");
			}
		} while (true);
	}
}