package com.team.enablement.plateform;

import com.team.enablement.plateform.util.GreetUser;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import static java.lang.System.exit;

public class WelcomePage
{
	final static String EST = "EST";
	final static String IST = "IST";
	final static String JST = "JST";

	public static void welcomePage()
	{
		final List<String> timeZoneList = new ArrayList<>(Arrays.asList(EST, IST, JST));
		System.out.println("\nWelcome \n ");

		do
		{
			System.out.println("\nPlease enter your country: ");
			System.out.println("1. US \n2. India \n3. Japan \n4. Exit");
			int countryInput = Math.toIntExact(ScannerUtil.UserInput());

			if (timeZoneList.size() >= countryInput && timeZoneList.get(countryInput - 1) != null)
			{
				System.out.println("\nGood " + GreetUser.greetUser(timeZoneList.get(countryInput - 1)));
				break;
			} else if (countryInput == 4)
			{
				exit(0);
			} else
			{
				System.out.println("\nInvalid input ");
			}
		} while (true);
	}
}
