package com.team.enablement.plateform;

import java.util.Scanner;

public class ScannerUtil
{
	private static final Scanner sc = new Scanner(System.in);

	public static Scanner getScanner()
	{
		return sc;
	}

	public static Long UserInput()
	{
		while (true)
		{
			try
			{
				return Long.parseLong(sc.nextLine().trim());
			} catch (NumberFormatException e)
			{
				System.out.println("Please Enter Proper Number...");
			}
		}
	}
}
